<?php
	error_reporting(E_ALL);
	mb_internal_encoding('UTF-8');

	try
	{
		// Подключаем файл конфигурации
		require_once 'Config.class.php';
		
		// Настраиваем автозагрузку классов
		spl_autoload_register(function ($class_name)
		{
			$directoryes = ['', 'libs', 'data'];

			foreach ($directoryes as $directory)
			{
				if (file_exists($class_path = Config::ROOT . $directory . '/' . $class_name . Config::EXT_CLASS))
				{
					require_once  $class_path;
				}
			}
		});
		
		// Создаем объект "гостевая книга"
		(new Guestbook(require_once 'config.php'))->init();
	}
	catch (PDOException $e)
	{
		exit('Ошибка подключения к базе данных');
	}