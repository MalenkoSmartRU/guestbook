<?php
	// Отвечает за подключение необходимых обработчиков
	class Router
	{
		protected $action;
		protected $valid = false;

		public function __construct()
		{
			$this->action= filter_input(INPUT_GET, 'a');
			$this->valid = $this->checkRoutes();
		}
		
		public function valid()
		{
			return $this->valid;
		}
		
		public function init()
		{
			
		}

		// Проверка пути на соответствие шаблону
		protected function checkRoutes()
		{
			return in_array($this->action, ['add', 'succ', 'adda']);
		}
	}
