<?php
    // Класс конфигурации
    abstract class Config
    {
        // Адреса файловой системы
        const ROOT = __DIR__ . '/';
        
        // Названия файлов
        const EXT_PHP = '.php';
        const EXT_CLASS = '.class' . self::EXT_PHP;
        const EXT_TPL = '.tpl';
    }
