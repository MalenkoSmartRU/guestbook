<?php
	// Класс гостевой книги
	class Guestbook extends Base
	{
		// Массив конфигурации
		protected $config;
		
		// Парсер адреса, вводимого пользователем
		protected $parser;
		
		// Список сообщений гостевой
		protected $messages = [];
		
		// Страницы гостевой
		protected $routes = ['add', 'addajax', 'success'];
		
		// Текущая страница гостевой
		protected $action = null;
		
		// Следует ли подключить шаблон?
		protected $show_template = true;
		
		// Ошибки
		protected $errors;
		// Значения, которые не были отправлены
		protected $empty_vals;
		
		public function __construct(array $config)
		{
			$this->config = $config;
			
			parent::__construct($this->config['db_host'], $this->config['db_name'], $this->config['db_user'], $this->config['db_password']);
			
			// Инициализация парсера
			$this->parser = new ParseUri;
			$this->action = $this->parser->getParam(0);
			
			// Получить список сообщений гостевой
			foreach ($a = self::$db->query('SELECT * FROM messages ORDER BY date DESC')->fetchAll() as $row)
			{
				$message = new Message();
				$message->setData($row['id_message'], $row['username'], $row['email'], $row['message'], $row['date'], $row['ip'], $row['browser']);
				$this->messages[] = $message;
			}
			
			// Инициализация классов ошибок
			$this->errors = new Errors();
			$this->empty_vals = new EmptyVals();
		}
		
		// Инициализация гостевой книги
		public function init()
		{
			$this->router();
			
			// Отобразить ли шаблон для данного запроса?
			if ($this->show_template)
			{
				$this->template();
			}
		}
		
		// Подключение шаблона
		protected function template()
		{
			require_once Config::ROOT . 'templates/guestbook.tpl';
		}
		
		// Добавить сообщение в гостевую книгу
		protected function addMessage(bool $ajax = false)
		{			
			// Используем recaptcha для проверки ввода
			$recaptcha = $this->recaptha(filter_input(INPUT_POST, 'g-recaptcha-response'));

			$added = Message::add($this->errors, $this->empty_vals, $recaptcha);
			
			if ($recaptcha === false)
			{
				$this->errors->add('Капча не была пройдена. Попробуйте еще раз.');
			}
			
			if ($ajax)
			{
				// Получаем ID последнего, внесенного в бд, сообщения
				$last_insert_id = self::$db->lastInsertId();
				
				// Получаем сообщение из базы данных
				$message = self::$db->query('SELECT username, email, message, date FROM messages WHERE id_message = '.$last_insert_id)->fetch();
				
				// Запаковываем данные в массив для отправки в json формате
				$jarray = [
					'errors' => $this->errors->getMessages(),
					'errors_count' => $this->errors->getCount(),
					'empty_vals' => $this->empty_vals->getMessages(),
					'empty_vals_count' => $this->empty_vals->getCount(),
					'username' => $message['username'],
					'email' => $message['email'],
					'message' => htmlspecialchars($message['message']),
					'date' => (new DateTime($message['date']))->format('d.m.Y H:i')
				];
				
				// Выводим данные в json формате
				echo json_encode($jarray, JSON_UNESCAPED_UNICODE);
			}
			else
			{
				if ($added)
				{
					header('Location: /success');
				}
			}
		}
		
		// Маршрутизатор
		protected function router()
		{
			// Если текущий экшен есть в массиве путей, то выполняем необходимый метод
			if (in_array($this->action, $this->routes))
			{
				switch ($this->action)
				{
					case 'add':
						$this->addMessage();
						break;
					case 'addajax':
						$this->addMessage(true);
						$this->show_template = false;
						break;
				}
			}
		}
		
		// Капча
		protected function recaptha($responce)
		{
			$curl = curl_init();
			
			// Необходимо послать запрос на сервера google
			if ($curl)
			{
				curl_setopt($curl, CURLOPT_URL, $this->config['recaptcha_url']);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
				curl_setopt($curl, CURLOPT_POST, true);
				curl_setopt($curl, CURLOPT_POSTFIELDS, 'secret='.$this->config['recaptcha_secret'].'&response='.$responce.'&remoteip='. filter_input(INPUT_SERVER, 'REMOTE_ADDR'));
				
				// Получить ответ
				$answer = curl_exec($curl);
				
				curl_close($curl);
				
				// И вернуть значение поля success
				return (bool)json_decode($answer, true)['success'];
			}
			else
			{
				return null;
			}
		}
	}
