<?php
    // Запись гостевой книги
    class Message extends Base
	{
		protected $id;
		protected $username;
		protected $email;
		protected $message;
		protected $date;
		protected $ip;
		protected $browser;
		
		public function __construct() {
			parent::__construct();
		}
		
		// Установка новых значений
		public function setData(int $id, string $username, string $email, string $message, string $date, int $ip, string $browser)
		{
			$this->id = $id;
			$this->username = $username;
			$this->email = $email;
			$this->message = $message;
			$this->date = $date;
			$this->ip = $ip;
			$this->browser = $browser;
		}
		
		// Получить данные		
		public function getUsername()
		{
			return $this->username;
		}
		
		public function getEmail()
		{
			return $this->email;
		}
		
		public function getMessage()
		{
			return $this->message;
		}
		
		public function getDate()
		{
			return $this->date;
		}
		
		// Добавить новое сообщение в базу данных
		public static function add(Errors &$errors, EmptyVals &$empty_vals, $recaptcha = true)
		{
			$validated = self::validation($errors, $empty_vals);
			
			// Если все входящие данные прошли проверку по ключам, то вносим в базу данных
			if ($validated && $recaptcha)
			{
				if (self::insert($validated['username'], $validated['email'], $validated['message']))
				{
					return true;
				}
				else
				{
					$errors->add('Ошибка при записи в базу данных. Попробуйте еще раз или свяжитесь с администратором.');
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		
		// Производит проверку введеных данных
		private static function validation(Errors &$errors, EmptyVals &$empty_vals)
		{
			$post = new Validation();
			
			for ($i = 0; $i <= count(self::keys()) - 1; $i++)
			{
				$post->validate(self::keys()[$i][0], self::keys()[$i][1], self::keys()[$i][2], self::keys()[$i][3], self::keys()[$i][4]);
			}
			
			if ($post->getErrors()->getCount() || $post->getEmptys()->getCount())
			{
				$errors = $post->getErrors();
				$empty_vals = $post->getEmptys();
				
				return null;
			}
			else
			{
				return $post->getValidated();
			}
		}
		
		// Вносит запись в базу данных
		private static function insert($username, $email, $message)
		{
			// Подготавливаем требуемые данные
			$ip = filter_input(INPUT_SERVER,'REMOTE_ADDR');
			$browser = filter_input(INPUT_SERVER,'HTTP_USER_AGENT');
			
			// Вносим запись в базу данных
			$request = self::$db->prepare('INSERT INTO messages VALUES (null,?,?,?,CURRENT_TIMESTAMP,INET_ATON(?),?)');
			
			if ($request->execute([$username, $email, $message, $ip, $browser]))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		private static function keys()
		{
			return
			[
				['username',	'/^[a-zа-я0-9\s]*$/iu', 'Поле имени заполнено неверно (русские и английские символы, цифры и пробел длиной не более 60 знаков и не менее 3)', 60, 3],
				['email',		'/^[а-яa-z0-9_\-\.]+@([а-яa-z0-9\-]+\.)+[а-яa-z]{2,10}$/iu', 'Поле email заполнено неверно (пример: email@gmail.com, не более 120 символов)', 120, 0],
				['message',		'/^.*$/usmi', 'Ваше сообщение имеет недопустимую длину (минимум 5 и максимум 1000 символов)', 1000, 5]
			];
		}
		
    }