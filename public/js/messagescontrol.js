/* global grecaptcha */

$(function() {
	var ajax_errors = $('#ajax-errors');
	var ajax_success = $('#ajax-success');
	
	// Предварительно скрыть ненужные элементы
	ajax_errors.addClass('hide');
	ajax_success.addClass('hide');

	$('#send').submit(function (e)
	{
		// Предварительно очистить и скрыть элементы
		ajax_errors.html(null);
		ajax_success.addClass('hide');

		e.preventDefault();
		
		$.ajax
		({
			url: '/addajax/',
			method: 'post',
			data: 'username=' + $('#username').val() + '&email=' + $('#email').val() + '&message=' + $('#message').val() + '&g-recaptcha-response=' + grecaptcha.getResponse(),
			success: function(data)
			{
				$('#ajax-alerts').removeClass('hide');
				
				var json = $.parseJSON(data);

				// Если запрос вернул ошибки, выводим этот участок кода
				if (json.errors_count || json.empty_vals_count)
				{
					// Вывод ошибок в блок
					json.errors.forEach(function(message)
					{
						ajax_errors.append('<p>'+message+'</p>');
					});
					
					json.empty_vals.forEach(function(message)
					{
						ajax_errors.append('<p>'+message+'</p>');
					});
					
					// Показываем блок, в который были добавлены ошибки
					ajax_errors.removeClass('hide');
				}
				// Если ошибок нет, то добавляем новое сообщение на страницу
				else
				{
					ajax_success.removeClass('hide');
					ajax_errors.addClass('hide');

					var message = $('#ajax-message').clone().removeClass('hide');
					
					message.find('.gb-message--avatar').text(json.username.substr(0,1));
					message.find('.gb-message--username').text(json.username);
					message.find('.gb-message--email').html('<a href="mailto:"'+json.email+">"+json.email+"</a>");
					message.find('.gb-message--message').text(json.message);
					message.find('.gb-message--time').text(json.date);
					
					$('#gb-messages').prepend(message);
					
					// Очистить поле ввода после успешной отправки
					$('.gb-form--message').val(null);
				}
			}
		});
		
		// После всех действий обновить капчу
		grecaptcha.reset();
	});
});
