<!DOCTYPE html>
<html lang="ru">
    <head>
        <title>Гостевая книга</title>
        <meta charset="utf-8" />
		<link rel="stylesheet" type="text/css" href="/css/reset.css">
        <link rel="stylesheet" type="text/css" href="/css/style.css">
		
		<script src='https://www.google.com/recaptcha/api.js'></script>
		<script src="/js/jquery-3.3.1.min.js"></script>
		<script src="/js/messagescontrol.js"></script>
    </head>
    <body>
		<div id="test">
		</div>
		<header class="main-header">
			<div class="box">
				<h1 class="block"><a href="/">Гостевая книга</a></h1>
			</div>
		</header>
		<main class="main-content">
			<div class="gb-form">
				<div class="box">
					<div id="ajax-alerts" class="block block_first hide">
						<div id="ajax-errors" class="errors hide"></div>
						<div id="ajax-success" class="success hide">Ваше сообщение было успешно добавлено!</div>
					</div>
					<?php if ($this->action): ?>
						<div class="block block_first">
							<?php if ($this->action == 'success'): ?>
								<div class="success">Ваше сообщение было успешно добавлено!</div>
							<?php endif; ?>
							<?php if ($this->errors->getCount()): ?>
								<div class="errors">
									<?php foreach($this->errors->getMessages() as $error): ?>
									<p><?=$error?></p>
									<?php endforeach; ?>
								</div>
							<?php endif; ?>
						</div>
					<?php endif; ?>
					<form method="post" action="/add/" id="send" class="gb-form--container block">
						<div class="gb-form--box-message">
							<input type="text" placeholder="Ваше имя" name="username" class="gb-form--username" id="username">
							<input type="text" placeholder="Ваш email" name="email" class="gb-form--email" id="email">
							<textarea placeholder="Ваше сообщение" name="message" class="gb-form--message" id="message"></textarea>
							<input type="submit" value="Добавить" class="gb-form--send">
						</div>
						<div class="gb-form--box-captcha">
							<div class="g-recaptcha" data-sitekey="<?=$this->config['recaptcha_sitekey']?>"></div>
						</div>
					</form>
				</div>
			</div>
					
			<div class="box">
				<div class="gb-messages block" id="gb-messages">
					<?php foreach($this->messages as $message): ?>
					<section class="gb-message">
						<div class="gb-message--avatar"><?=mb_substr($message->getUsername(), 0, 1)?></div>
						<div class="box">
							<h2 class="gb-message--username"><?=$message->getUsername()?></h2>
							<div class="gb-message--email"><a href="mailto:<?=$message->getEmail()?>"><?=$message->getEmail()?></a></div>
							<div class="gb-message--message"><?=htmlspecialchars($message->getMessage())?></div>
							<div class="gb-message--time"><?=(new DateTime($message->getDate()))->format('d.m.Y H:i')?></div>
						</div>
					</section>
					<?php endforeach; ?>
				</div>
			</div>
		</main>
		<footer class="main-footer">
			<div class="box">
				<div class="block">
					<?=date('Y')?>
				</div>
			</div>
		</footer>
		
		<!--Дополнительная секция для манипулирования с помощью jquery + ajax-->
		<section class="gb-message hide" id="ajax-message">
			<div class="gb-message--avatar"></div>
			<div class="box">
				<h2 class="gb-message--username">AJAX</h2>
				<div class="gb-message--email"></div>
				<div class="gb-message--message"></div>
				<div class="gb-message--time"></div>
			</div>
		</section>
    </body>
</html>