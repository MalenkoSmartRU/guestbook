# Установка и настройка гостевой книги

Перед установкой убедитесь, что у вас установлен ***[**Docker**](http://www.docker.com)*** последней версии и **docker-compose**.

---

## 1. Запуск конфигурации

Для запуска связки необходимых программ, откройте терминал и перейдите по адресу "***ваш_проект/laradock-guestbook***" в корне проекта и затем используйте следующую команду:

```docker-compose up -d nginx mysql```
	
Рекомендуется использовать phpmyadmin или adminer для работы с базой данных. Запуск можно осуществить следующей командой:
	
```docker-compose up -d phpmyadmin```

Если вы делаете настройку под Linux, то данные операции потребуют прав администатора. Для этого предваряйте каждую команду выражением sudo, например: 
	
```sudo docker-compose up -d nginx mysql phpmyadmin```

---

## 2. Настройка таблицы в базе данных и ее подключение

Создайте в вашей базе данных новую базу или перейдите в уже существующую.
Используйте данный SQL-запрос для создания пустой таблицы **messages**:
	
```
CREATE TABLE `messages` (
  `id_message` int(11) UNSIGNED NOT NULL,
  `username` varchar(60) NOT NULL,
  `email` varchar(120) DEFAULT NULL,
  `message` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip` int(11) UNSIGNED DEFAULT NULL,
  `browser` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

ALTER TABLE `messages`
  ADD PRIMARY KEY (`id_message`);
  
ALTER TABLE `messages`
  MODIFY `id_message` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
```

---

Для подключения к базе данных в корне проекта найдите файл **config.php**.
Замените значения переменных (расположены в угловых скобках) ***db_host***, ***db_name***, ***db_user***, ***db_password*** на свои:
```
'db_host'			=> '<значение>',
'db_name'			=> '<значение>',
'db_user'			=> '<значение>',
'db_password'		=> '<значение>',
```

Например:
```
'db_host'			=> 'mysql',
'db_name'			=> 'guestbook',
'db_user'			=> 'root',
'db_password'		=> 'root',
```
---

## 3. Подключение Google reCAPTCHA

Для первоначальной настройки воспользуйтесь документацией по ***[reCAPTCHA](https://developers.google.com/recaptcha/)***.
В файле **config.php** замените следующие значения на свои, которые предоставит вам сервис reCAPTCHA:
```
'recaptcha_url'		=> '<значение>',
'recaptcha_secret'	=> '<значение>',
'recaptcha_sitekey' => '<значение>'
```

Например:
```
'recaptcha_url'		=> 'https://www.google.com/recaptcha/api/siteverify',
'recaptcha_secret'	=> '6LezP0MUAAAAAJzf5X-jY4_vSz4pbb4QvhKTAaeu',
'recaptcha_sitekey' => '6LezP0MUAAAAAPtPNyZlQHzJfS7Cu9tPVRh1u6Vl'
```
---

## 4. Завершение

Теперь гостевая книга доступна по адресу http://localhost/.

